using System;

namespace zinger_test
{
    class Program
    {
        static void Main(string[] args)
        {
            Boolean lockedState = false; // заблокирована ли смена цвета
            string currentColor = "blue"; // текущий цвет

            Console.WriteLine("Введите количество сообщений");
            var number = int.Parse(Console.ReadLine());

            Console.WriteLine("OK, погнали. Нужно ввести " + number + " строк");
            for(var i = 1; i <= number; i++)
            {
                Console.WriteLine("Введите " + i + " значение");
                var input = Console.ReadLine();
                if (input.Equals("lock"))
                {
                    lockedState = true;
                    continue;
                }

                if (input.Equals("unlock"))
                {
                    lockedState = false;
                    continue;
                }

                if (!lockedState && !currentColor.Equals(input))
                {
                    currentColor = input;
                }

            }
            Console.WriteLine("Итоговый цвет: " + currentColor);

        }
    }
}

